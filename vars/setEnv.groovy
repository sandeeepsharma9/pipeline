#!groovy

def call(body) 
{
   def config = [:]
   body.resolveStrategy = Closure.DELEGATE_FIRST
   body.delegate = config
   body()
   timestamps {
     currentBuild.result = "SUCCESS"
     try {
      build_options = new ChoiceParameterDefinition('BUILD_OPTIONS', config.BUILD_OPTIONS as String[], 'Build And Deploy')
      build_value = input(message:'Please Choose', parameters: [build_options])
      env.BUILD_OPTIONS = build_value
     }
     catch(Exception caughtError) {
      print "[Error]: environment is not set"
      currentBuild.result = "FAILURE"
      throw caughtError
     }
  }
}
