#!grovvy
def call(body) {
  def config = [:]
  body.resolveStrategy = Closure.DELEGATE_FIRST
  body.delegate = config
  body()
  timestamps {
    currentBuild.result = "SUCCESS"
    try {
      stage('Speedy UI') {
        def UI_ACTION
        parallel (
          "Code Checkout" : {
            try {
              print "[INFO]: code checkout...."
              checkout([$class: 'GitSCM', branches: [[name: "*/${config.BRANCH}"]], doGenerateSubmoduleConfigurations: false, extensions: [], submoduleCfg: [], userRemoteConfigs: [[credentialsId: "${config.GIT_CREDENTIALS}", url: "${config.GIT_URL}"]]])
              UI_ACTION = "checkout"
            }
            catch(Exception caughtError) {
              print "[ERROR]: code checkout is failed...."
              currentBuild.result = "FAILURE"
              throw caughtError
            }
          },

          "NPM Install" : {
            try{
              while (UI_ACTION != "checkout") {
                continue
              }
              dir('UI') {
                print "[INFO]: executing npm install command...."
                sh 'npm install'
                UI_ACTION = "npm"
              }
            }
            catch(Exception caughtError) {
              print "[ERROR]: npm install command is not executed successfully...."
              currentBuild.result = "FAILURE"
              throw caughtError
            }
          },

          'BOWER Install' : {
            try{
              while (UI_ACTION != "npm") {
                continue
              }
              dir('UI') {
                print "[INFO]: executing bower install command...."
                sh 'bower install'
                UI_ACTION = "bower"
              }
            }
            catch(Exception caughtError) {
              print "[ERROR]: bower install command is not executed successfully...."
              currentBuild.result = "FAILURE"
              throw caughtError
            }
          },

          'UI Code Build' : {
            try{
              while (UI_ACTION != "bower") {
                continue
              }
              dir('UI') {
                print "[INFO]: doning UI code build...."
                sh 'gulp clean build'
              }
            }
            catch(Exception caughtError) {
              print "[ERROR]: UI code build is done successfully...."
              currentBuild.result = "FAILURE"
              throw caughtError
            }
          }
        )
      }
    }
    catch(Exception caugthError) {
      print "[ERROR]: failed to run UI build and deploy pipeline"
      currentBuild.result = "FAILURE"
      throw caugthError
    }
    finally {
      try {
        STATUS = "${currentBuild.result}"
        SUBJECT = "${env.JOB_NAME} | ${STATUS}"
        BODY = "this is test mail"
        emailext body: "${BODY}", recipientProviders: [[$class: 'RequesterRecipientProvider'], [$class: 'CulpritsRecipientProvider']], subject: "${SUBJECT}", to: "${config.RECIPIENT}"
      }
      catch(Exception caughtError){
        print "[ERRIR]: failed to send email notification...."
        currentBuild.result = "FAILURE"
        throw caughtError
      }
    }
  }
}
